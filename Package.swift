// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftPackageTest",
    products: [
        .library(name: "SwiftPackageTest", targets: ["SwiftPackageTest"]),
    ], dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(url: "https://github.com/SwiftyJSON/SwiftyJSON.git", from: "4.0.0"),
        .package(url: "https://github.com/Alamofire/Alamofire.git", from: "5.4.3")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target( name: "SwiftPackageTest", dependencies: ["SwiftyJSON", "Alamofire"]),
        .testTarget(
            name: "SwiftPackageTestTests",
            dependencies: ["SwiftPackageTest"]),
    ]
)
