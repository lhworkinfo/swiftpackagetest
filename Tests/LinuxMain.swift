import XCTest

import SwiftPackageTestTests

var tests = [XCTestCaseEntry]()
tests += SwiftPackageTestTests.allTests()
XCTMain(tests)
